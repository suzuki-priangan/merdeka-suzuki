<?php include "../koneksi.php" ?>
<?php include "head.php" ?>

<body>
    <div id="wrapper">
    <?php include "nav.php" ?>
        <div id="page-wrapper" class="gray-bg">
        <div class="row border-bottom">
        <nav class="navbar navbar-static-top" role="navigation" style="margin-bottom: 0">
        <div class="navbar-header">
            <a class="navbar-minimalize minimalize-styl-2 btn btn-primary " href="#"><i class="fa fa-bars"></i> </a>
            <form role="search" class="navbar-form-custom" action="">
            </form>
        </div>
            <ul class="nav navbar-top-links navbar-right">
                <li>
                    <a href="../keluar.php">
                        <i class="fa fa-sign-out"></i> Log out
                    </a>
                </li>
            </ul>

        </nav>
        </div>
            <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-lg-10">
                    <h2>Detail Profil Konsumen</h2>
                    <ol class="breadcrumb">
                        <li>
                            <a href="../index.php">Home</a>
                        </li>
                        <li>
                            <a>Profil Konsumen</a>
                        </li>
                        <li class="active">
                            <strong>Detail Profil Konsumen</strong>
                        </li>
                    </ol>
                </div>
                <div class="col-lg-2">

                </div>
            </div>
        <div class="wrapper wrapper-content animated fadeInRight">
            <div class="row">

                <div class="col-lg-5">

                </div>
            </div>

            <div class="row">
                <div class="col-lg-4">

                </div>
        </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="ibox float-e-margins">
                        <div class="ibox-title">
                            <h5>Detail Profil Konsumen </h5>
                        </div>
                        <div class="ibox-content">
                            
                            <?php
                                include '../koneksi.php';
                                
                                $id_konsumen = $_GET['id_konsumen'];
                                $data = mysqli_query($con, "select * from konsumen where id_konsumen='$id_konsumen'");
                                while(@$d = mysqli_fetch_array($data)){
                            ?>

                            <form action="#" method="post" class="form-horizontal" enctype="multipart/form-data">
                                <input type="hidden" name="id_konsumen" value="<?php echo $id_konsumen; ?>">

                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Nama Konsumen</label>
                                    <div class="col-sm-10">
                                        <input type="text" name="nama_konsumen" class="form-control" value="<?php echo $d['nama_konsumen']; ?>" readonly>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Foto</label>
                                    <div class="col-sm-10">
                                        <a href="../../img/foto_pelanggan/<?php echo $d['foto'] ?>">
                                            <img src='../../img/foto_pelanggan/<?php echo $d['foto'] ?>' width='100px' height='80px'/>
                                        </a>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Foto KTP</label>
                                    <div class="col-sm-10">
                                        <a href="../../img/foto_ktp_pelanggan/<?php echo $d['foto_ktp'] ?>">
                                            <img src='../../img/foto_ktp_pelanggan/<?php echo $d['foto_ktp'] ?>' width='100px' height='80px'/>
                                        </a>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Nomer KTP</label>
                                    <div class="col-sm-10">
                                        <input type="text" name="no_ktp" class="form-control" value="<?php echo $d['no_ktp']; ?>" readonly>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Tempat</label>
                                        <div class="col-sm-10">
                                        <input type="text" name="tempat" class="form-control" value="<?php echo $d['tempat']; ?>" readonly>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Tanggal Lahir</label>
                                    <div class="col-sm-10">
                                        <input type="text" name="tgl_lahir" class="form-control" value="<?php echo $d['tgl_lahir']; ?>" readonly>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Jenis Kelamin</label>
                                    <div class="col-sm-10">
                                        <input type="text" name="jenis_kelamin" class="form-control" value="<?php echo $d['jenis_kelamin']; ?>" readonly>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Phone Number</label>
                                    <div class="col-sm-10">
                                        <input type="text" name="phone_number" class="form-control" value="<?php echo $d['phone_number']; ?>" readonly>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Email</label>
                                    <div class="col-sm-10">
                                        <input type="text" name="email" class="form-control" value="<?php echo $d['email']; ?>" readonly>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Siup</label>
                                    <div class="col-sm-10">
                                        <input type="text" name="siup" class="form-control" value="<?php echo $d['siup']; ?>" readonly>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Kode Referal</label>
                                    <div class="col-sm-10">
                                        <input type="text" name="kode_referal" class="form-control" value="<?php echo $d['kode_referal']; ?>" readonly>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Username</label>
                                    <div class="col-sm-10">
                                        <input type="text" name="username" class="form-control" value="<?php echo $d['username']; ?>" readonly>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Password</label>
                                    <div class="col-sm-10">
                                        <input type="text" name="password" class="form-control" value="<?php echo $d['password']; ?>" readonly>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Aktif</label>
                                    <div class="col-sm-10">
                                        <input type="text" name="aktif" class="form-control" value="<?php echo $d['aktif']; ?>" readonly>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Kode</label>
                                    <div class="col-sm-10">
                                        <input type="text" name="kode" class="form-control" value="<?php echo $d['kode']; ?>" readonly>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-4 col-sm-offset-2">
                                        <a href="index.php" type="submit" class="btn btn-primary"><i class="glyphicon glyphicon-chevron-left"></i> Kembai</a>
                                    </div>
                                </div>
                            </form>
                            <?php 
                            }
                            ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
            
            <?php include "footer.php" ?>

        </div>
        </div>

    <script src="js/jquery-3.1.1.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/plugins/metisMenu/jquery.metisMenu.js"></script>
    <script src="js/plugins/slimscroll/jquery.slimscroll.min.js"></script>
    <script src="js/inspinia.js"></script>
    <script src="js/plugins/pace/pace.min.js"></script>
    <script src="js/plugins/iCheck/icheck.min.js"></script>
        <script>
            $(document).ready(function () {
                $('.i-checks').iCheck({
                    checkboxClass: 'icheckbox_square-green',
                    radioClass: 'iradio_square-green',
                });
            });
        </script>
</body>
</html>
