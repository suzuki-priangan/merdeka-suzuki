<?php include "../koneksi.php" ?>
<?php include "head.php" ?>

<body>
    <div id="wrapper">
    <?php include "nav.php" ?>
        <div id="page-wrapper" class="gray-bg">
        <div class="row border-bottom">
        <nav class="navbar navbar-static-top" role="navigation" style="margin-bottom: 0">
        <div class="navbar-header">
            <a class="navbar-minimalize minimalize-styl-2 btn btn-primary " href="#"><i class="fa fa-bars"></i> </a>
            <form role="search" class="navbar-form-custom" action="">
            </form>
        </div>
            <ul class="nav navbar-top-links navbar-right">
                <li>
                    <a href="../keluar.php">
                        <i class="fa fa-sign-out"></i> Log out
                    </a>
                </li>
            </ul>

        </nav>
        </div>
            <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-lg-10">
                    <h2>Detail Mobil</h2>
                    <ol class="breadcrumb">
                        <li>
                            <a href="../index.php">Home</a>
                        </li>
                        <li>
                            <a>Mobil</a>
                        </li>
                        <li class="active">
                            <strong>Detail Mobil</strong>
                        </li>
                    </ol>
                </div>
                <div class="col-lg-2">

                </div>
            </div>
        <div class="wrapper wrapper-content animated fadeInRight">
            <div class="row">

                <div class="col-lg-5">

                </div>
            </div>

            <div class="row">
                <div class="col-lg-4">

                </div>
        </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="ibox float-e-margins">
                        <div class="ibox-title">
                            <h5>Detail Mobil </h5>
                        </div>
                        <div class="ibox-content">
                        	
                        	<?php
                                include '../koneksi.php';
                                
                                $id_detail_mobil = $_GET['id_detail_mobil'];
                                $data = mysqli_query($con, "select * from detail_mobil where id_detail_mobil='$id_detail_mobil'");
                                while(@$d = mysqli_fetch_array($data)){
                            ?>

                            <form action="#" method="post" class="form-horizontal" enctype="multipart/form-data">
                            	<input type="hidden" name="id_detail_mobil" value="<?php echo $id_detail_mobil; ?>">

                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Jenis Mobil</label>
                                    <div class="col-sm-10">
                                        <input type="text" name="jenis_mobil" class="form-control" value="<?php echo $d['jenis_mobil']; ?>" readonly>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Tipe</label>
                                        <div class="col-sm-10">
                                        <input type="text" name="tipe" class="form-control" value="<?php echo $d['tipe']; ?>" readonly>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Tahun Produksi</label>
                                    <div class="col-sm-10">
                                        <input type="text" name="thn_produksi" class="form-control" value="<?php echo $d['thn_produksi']; ?>" readonly>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Stok</label>
                                    <div class="col-sm-10">
                                        <input type="text" name="stok" class="form-control" value="<?php echo $d['stok']; ?>" readonly>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Image</label>
                                    <div class="col-sm-10">
                                        <a href="../../img/mobil/<?php echo $d['img'] ?>">
                                            <img src='../../img/mobil/<?php echo $d['img'] ?>' width='100px' height='80px'/>
                                        </a>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Spesifikasi</label>
                                    <div class="col-sm-10">
                                        <input type="text" name="spesifikasi" class="form-control" value="<?php echo $d['spesifikasi']; ?>" readonly>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Link</label>
                                    <div class="col-sm-10">
                                        <input type="text" name="link" class="form-control" value="<?php echo $d['link']; ?>" readonly>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-4 col-sm-offset-2">
                                        <button class="btn btn-primary" name="save" type="submit"><i class="glyphicon glyphicon-saved"></i> Update</button>
                                        
                                        <a href="index.php" type="submit" class="btn btn-warning"><i class="glyphicon glyphicon-remove"></i> Cancel</a>
                                    </div>
                                </div>
                            </form>
                            <?php 
                            }
                            ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
            
            <?php include "footer.php" ?>

        </div>
        </div>

    <script src="js/jquery-3.1.1.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/plugins/metisMenu/jquery.metisMenu.js"></script>
    <script src="js/plugins/slimscroll/jquery.slimscroll.min.js"></script>
    <script src="js/inspinia.js"></script>
    <script src="js/plugins/pace/pace.min.js"></script>
    <script src="js/plugins/iCheck/icheck.min.js"></script>
        <script>
            $(document).ready(function () {
                $('.i-checks').iCheck({
                    checkboxClass: 'icheckbox_square-green',
                    radioClass: 'iradio_square-green',
                });
            });
        </script>
</body>
</html>
