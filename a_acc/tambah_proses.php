<?php
include('../koneksi.php');
// ambil data file
$filename 			= $_FILES["gambar_acc"]["name"];
$file_basename 		= substr($filename, 0, strripos($filename, '.'));
$file_ext 			= substr($filename, strripos($filename, '.'));
$filesize 			= $_FILES["gambar_acc"]["size"];
$allowed_file_types = array('.jpg','.png','.jpeg','.gif','.JPG','.JPEG  ');

if (in_array($file_ext,$allowed_file_types) && ($filesize < 200000000))
{
	// Ubah nama file
	$newfilename = md5($file_basename) . $file_ext;
	if (file_exists("../../img/aksesoris/" . $newfilename))
	{
		// Jika file sudah ada
		echo "<script>window.alert('File sudah ada!'); window.location.href='tambah.php'</script>";
	}
	else
	{
		move_uploaded_file($_FILES["gambar_acc"]["tmp_name"], "../../img/aksesoris/" . $newfilename);

    // ambil data dari form
    $gambar_acc 	= $newfilename;
    $kategori 		= $_POST['kategori'];
    $nama_acc		= $_POST['nama_acc'];
    $merk_acc 		= $_POST['merk_acc'];
    $harga_acc 		= $_POST['harga_acc'];
    $deskripsi 		= $_POST['deskripsi'];
    $stok 			= $_POST['stok'];

    // query
    $query = "INSERT INTO `aksesoris` (`id_aksesoris`, `kategori`, `gambar_acc`, `nama_acc`, `merk_acc`, `harga_acc`, `deskripsi`, `stok`) VALUES (NULL, '$kategori', '$gambar_acc', '$nama_acc', '$merk_acc', '$harga_acc', '$deskripsi', '$stok');";

    $hasil = mysqli_query($con, $query);

    // cek keberhasilan pendambahan data
    if ($hasil == true) {
      echo "<script>window.alert('Tambah Data Berhasil'); window.location.href='index.php'</script>";
    } else {
      echo "<script>window.alert('Tambah Data Gagal!'); window.location.href='tambah.php'</script>";
    }

	}
}
elseif (empty($file_basename))
{
	// file belum dipilih
	echo "<script>window.alert('Pilih file untuk diunggah'); window.location.href='tambah.php'</script>";
}
elseif ($filesize > 2000000000)
{
	// ukuran file terlalu besar
	echo "<script>window.alert('File Terlalu Besar!'); window.location.href='tambah.php'</script>";
}
else
{
	// format file bukan gambar
	echo "<script>window.alert('Format File Salah'); window.location.href='tambah.php'</script>";
	unlink($_FILES["gambar_acc"]["tmp_name"]);
}
?>