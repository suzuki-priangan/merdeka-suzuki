<?php
include('../koneksi.php');
$id_sparepart = $_POST['id_sparepart'];

$filename           = $_FILES['img']["name"];
$file_basename      = substr($filename, 0, strripos($filename, '.'));
$file_ext           = substr($filename, strripos($filename, '.'));
$filesize           = $_FILES['img']["size"];
$allowed_file_types = array('.jpg','.png','.jpeg','.gif','.JPG','.JPEG');

if (in_array($file_ext,$allowed_file_types) && ($filesize < 200000000))
{
    // Ubah nama file
    $newfilename = ($file_basename) . $file_ext;
    if (file_exists("../../img/sparepart/" . $newfilename))
    {
        // Jika file sudah ada
        echo "<script>window.alert('File sudah ada!'); window.location.href='edit.php'</script>";
    }
    else
    {
        move_uploaded_file($_FILES["img"]["tmp_name"], "../../img/sparepart/" . $newfilename);

    // ambil data dari form
    $img            = $newfilename;
    $SN 			= $_POST['SN/PN'];
    $merk			= $_POST['merk'];
    $kegunaan 		= $_POST['kegunaan'];
    $jumlah 		= $_POST['jumlah'];
    $harga 			= $_POST['harga'];

    // query
    $query = "UPDATE sparepart SET SN/PN='$SN', merk='$merk', kegunaan='$kegunaan', jumlah='$jumlah', harga='$harga' WHERE id_sparepart='$id_sparepart'";

    $hasil = mysqli_query($con, $query);

    // cek keberhasilan pendambahan data
    if ($hasil == true) {
      echo "<script>window.alert('Data Berhasil Di Upadate'); window.location.href='index.php'</script>";
    } else {
      echo "<script>window.alert('Data Gagal Di Upadate'); window.location.href='edit.php'</script>";
    }

    }
}
elseif (empty($filename))
{
    $SN 			= $_POST['SN/PN'];
    $merk			= $_POST['merk'];
    $kegunaan 		= $_POST['kegunaan'];
    $jumlah 		= $_POST['jumlah'];
    $harga 			= $_POST['harga'];
    $img            = $_POST['img'];
    
    $query = "UPDATE sparepart SET SN/PN='$SN', merk='$merk', kegunaan='$kegunaan', jumlah='$jumlah', harga='$harga', img='$img' WHERE id_sparepart='$id_sparepart'";

$hasil = mysqli_query($con, $query);
    if ($hasil == true) {
      echo "<script>window.alert('Data Berhasil Di Update'); window.location.href='index.php'</script>";
    } else {
      echo "<script>window.alert('Data Gagal Di Update'); window.location.href='edit.php'</script>";
    }
    
}
elseif ($filesize > 2000000000)
{
    // ukuran file terlalu besar
    echo "<script>window.alert('File yang diunggah terlalu besar!'); window.location.href='edit.php?id_sparepart=$id_sparepart'</script>";
}
else
{
    // format file bukan gambar
    echo "<script>window.alert('Format file salah'); window.location.href='edit.php?id_sparepart=$id_sparepart'</script>";
    unlink($_FILES["img"]["tmp_name"]);
}