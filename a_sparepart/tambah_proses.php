<?php
include('../koneksi.php');
// ambil data file
$filename 			= $_FILES["img"]["name"];
$file_basename 		= substr($filename, 0, strripos($filename, '.'));
$file_ext 			= substr($filename, strripos($filename, '.'));
$filesize 			= $_FILES["img"]["size"];
$allowed_file_types = array('.jpg','.png','.jpeg','.gif','.JPG','.JPEG  ');

if (in_array($file_ext,$allowed_file_types) && ($filesize < 200000000))
{
	// Ubah nama file
	$newfilename = md5($file_basename) . $file_ext;
	if (file_exists("../../img/sparepart/" . $newfilename))
	{
		// Jika file sudah ada
		echo "<script>window.alert('File sudah ada!'); window.location.href='tambah.php'</script>";
	}
	else
	{
		move_uploaded_file($_FILES["img"]["tmp_name"], "../../img/sparepart/" . $newfilename);

    // ambil data dari form
    $img 			= $newfilename;
    $SN 			= $_POST['SN/PN'];
    $merk			= $_POST['merk'];
    $kegunaan 		= $_POST['kegunaan'];
    $jumlah 		= $_POST['jumlah'];
    $harga 			= $_POST['harga'];

    // query
    $query = "INSERT INTO `sparepart` (`id_sparepart`, `SN/PN`, `merk`, `kegunaan`, `img`, `jumlah`, `harga`) VALUES (NULL, '$SN', '$merk', '$kegunaan', '$img', '$jumlah', '$harga');";

    $hasil = mysqli_query($con, $query);

    // cek keberhasilan pendambahan data
    if ($hasil == true) {
      echo "<script>window.alert('Tambah Data Berhasil'); window.location.href='index.php'</script>";
    } else {
      echo "<script>window.alert('Tambah Data Gagal!'); window.location.href='tambah.php'</script>";
    }

	}
}
elseif (empty($file_basename))
{
	// file belum dipilih
	echo "<script>window.alert('Pilih file untuk diunggah'); window.location.href='tambah.php'</script>";
}
elseif ($filesize > 2000000000)
{
	// ukuran file terlalu besar
	echo "<script>window.alert('File Terlalu Besar!'); window.location.href='tambah.php'</script>";
}
else
{
	// format file bukan gambar
	echo "<script>window.alert('Format File Salah'); window.location.href='tambah.php'</script>";
	unlink($_FILES["img"]["tmp_name"]);
}
?>