<nav class="navbar-default navbar-static-side" role="navigation">
            <div class="sidebar-collapse">
                <ul class="nav metismenu" id="side-menu">
                    <li class="nav-header">
                        <div class="logo-element">
                            MENU
                        </div>
                    </li>
                    <li>
                        <a href="../index.php"><i class="fa fa-th-large"></i> <span class="nav-label">Dashboard</span></a>
                    </li>
                    <li>
                        <a href=""><i class="fa fa-sitemap"></i> <span class="nav-label">Data Stok</span> <span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level collapse">
                            <li><a href="../a_mobil/index.php">Mobil</a></li>
                            <li><a href="../a_detail_mobil/index.php">Detail Mobil</a></li>
                            <li><a href="../a_acc/index.php">Aksesoris</a></li>
                            <li><a href="../a_sparepart/index.php">Sparepart</a></li>
                            <li><a href="../a_stok/index.php">Stok</a></li>
                            <li><a href="../a_dp/index.php">DP</a></li>
                            <li><a href="index.php">Karoseri</a></li>
                        </ul>
                    </li>
                    <li>
                        <a href=""><i class="fa fa-sitemap"></i> <span class="nav-label">Data Konsumen</span> <span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level collapse">
                            <li><a href="../a_profil_konsumen/index.php">Profil Konsumen</a></li>
                            <li><a href="../a_perorangan/index.php">Kredit Perorangan</a></li>
                            <li><a href="../a_perusahaan/index.php">Kredit Perusahaan</a></li>
                            <li><a href="../a_pemesanan_aksesoris/index.php">Pemesanan Aksesoris</a></li>
                        </ul>
                    </li>
                    <li>
                        <a href=""><i class="fa fa-sitemap"></i> <span class="nav-label">Data Transaksi</span> <span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level collapse">
                            <li><a href="../a_cash_perorangan/index.php">Cash Perorangan</a></li>
                            <li><a href="../a_cash_perusahaan/index.php">Cash Perusahaan</a></li>
                            <li><a href="../a_cash_karoseri_perorangan/index.php">Cash Karoseri Perorangan</a></li>
                            <li><a href="../a_cash_karoseri_perusahaan/index.php">Cash Karoseri Perusahaan</a></li>
                        </ul>
                    </li>
                    <li>
                        <a href=""><i class="fa fa-sitemap"></i> <span class="nav-label">Data Layanan</span> <span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level collapse">
                            <li><a href="../a_bookingtestdrive/index.php">Booking Test Drive</a></li>
                            <li><a href="../a_bookingservice/index.php">Booking Service</a></li>
                            <li><a href="../a_perawatan/index.php">Perawatan</a></li>
                        </ul>
                    </li>
                    <li>
                        <a href=""><i class="fa fa-sitemap"></i> <span class="nav-label">Konten Manager</span> <span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level collapse">
                            <li><a href="../a_artikel/index.php">Artikel</a></li>
                            <li><a href="../a_tips_trik/index.php">Tips & Trik</a></li>
                            <li><a href="../a_testimoni/index.php">Testimoni</a></li>
                        </ul>
                    </li>
                    <li>
                        <a href=""><i class="fa fa-sitemap"></i> <span class="nav-label">Data Manager</span> <span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level collapse">
                            <li><a href="../a_tradein_perorangan/index.php">Tradein Perorangan</a></li>
                            <li><a href="../a_tradein_perusahaan/index.php">Tradein Perusahaan</a></li>
                        </ul>
                    </li>
                    <li>
                        <a href=""><i class="fa fa-sitemap"></i> <span class="nav-label">User Manager</span> <span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level collapse">
                            <li><a href="../a_user/index.php">Data User</a></li>
                        </ul>
                    </li>
                </ul>
            </div>
        </nav>