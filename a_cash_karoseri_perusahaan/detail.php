<?php include "head.php" ?>
<body>
    <div id="wrapper">
    <?php include "nav.php" ?>
        <div id="page-wrapper" class="gray-bg">
        <div class="row border-bottom">
        <nav class="navbar navbar-static-top" role="navigation" style="margin-bottom: 0">
        <div class="navbar-header">
            <a class="navbar-minimalize minimalize-styl-2 btn btn-primary " href="#"><i class="fa fa-bars"></i> </a>
            <form role="search" class="navbar-form-custom" action="">
            </form>
        </div>
            <ul class="nav navbar-top-links navbar-right">
                <li>
                    <a href="../keluar.php">
                        <i class="fa fa-sign-out"></i> Log out
                    </a>
                </li>
            </ul>

        </nav>
        </div>
            <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-lg-10">
                    <h2>Detail Cash Karoseri Perusahaan</h2>
                    <ol class="breadcrumb">
                        <li>
                            <a href="../index.php">Home</a>
                        </li>
                        <li>
                            <a>Pemesanan</a>
                        </li>
                        <li class="active">
                            <strong>Detail Cash Karoseri Perusahaan</strong>
                        </li>
                    </ol>
                </div>
                <div class="col-lg-2">

                </div>
            </div>
        <div class="wrapper wrapper-content animated fadeInRight">
            <div class="row">

                <div class="col-lg-5">

                </div>
            </div>

            <div class="row">
                <div class="col-lg-4">

                </div>
        </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="ibox float-e-margins">
                        <div class="ibox-title">
                            <h5>Detail Cash Karoseri Perusahaan</h5>
                        </div>
                        <div class="ibox-content">
                            
                            <?php
                                include '../koneksi.php';
                                
                                $id_cash = $_GET['id_cash'];
                                $data = mysqli_query($con, "select * from cash_karoseri_perusahaan
                                    ");
                                while($d = mysqli_fetch_array($data)){
                            ?>

                            <form action="" method="post" class="form-horizontal">
                                <input type="hidden" name="id_cash" value="<?php echo $id_cash; ?>">

                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Nama Perusahaan</label>
                                    <div class="col-sm-10">
                                        <input type="text" name="nama_perusahaan" class="form-control" value="<?php echo $d['nama_perusahaan']; ?>" readonly>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Nomer HP</label>
                                        <div class="col-sm-10">
                                            <input type="text" name="no_hp" class="form-control" value="<?php echo $d['no_hp']; ?>" readonly>
                                        </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Email</label>
                                        <div class="col-sm-10">
                                            <input type="text" name="email" class="form-control" value="<?php echo $d['email']; ?>" readonly>
                                        </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Banyak</label>
                                        <div class="col-sm-10">
                                            <input type="text" name="banyak" class="form-control" value="<?php echo $d['banyak']; ?>" readonly>
                                        </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Kabupaten</label>
                                        <div class="col-sm-10">
                                            <input type="text" name="kabupaten" class="form-control" value="<?php echo $d['kabupaten']; ?>" readonly>
                                        </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Kecamatan</label>
                                        <div class="col-sm-10">
                                            <input type="text" name="kecamatan" class="form-control" value="<?php echo $d['kecamatan']; ?>" readonly>
                                        </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Bidang Usaha</label>
                                        <div class="col-sm-10">
                                            <input type="text" name="bidang_usaha" class="form-control" value="<?php echo $d['bidang_usaha']; ?>" readonly>
                                        </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Nomer Akta</label>
                                        <div class="col-sm-10">
                                            <input type="text" name="no_akta" class="form-control" value="<?php echo $d['no_akta']; ?>" readonly>
                                        </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Bentuk Usaha</label>
                                        <div class="col-sm-10">
                                            <input type="text" name="bentuk_usaha" class="form-control" value="<?php echo $d['bentuk_usaha']; ?>" readonly>
                                        </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Tipe Transaksi</label>
                                        <div class="col-sm-10">
                                            <input type="text" name="tipe_transaksi" class="form-control" value="<?php echo $d['tipe_transaksi']; ?>" readonly>
                                        </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Setuju</label>
                                        <div class="col-sm-10">
                                            <input type="text" name="setuju" class="form-control" value="<?php echo $d['setuju']; ?>" readonly>
                                        </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Status</label>
                                        <div class="col-sm-10">
                                            <input type="text" name="status" class="form-control" value="<?php echo $d['status']; ?>" readonly>
                                        </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-4 col-sm-offset-2">
                                        <a href="index.php" class="btn btn-info"><i class="glyphicon glyphicon-fast-backward"></i> Back</a>
                                    </div>
                                </div>
                            </form>
                            <?php 
                            }
                            ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
            
            <?php include "footer.php" ?>

        </div>
        </div>

    <script src="js/jquery-3.1.1.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/plugins/metisMenu/jquery.metisMenu.js"></script>
    <script src="js/plugins/slimscroll/jquery.slimscroll.min.js"></script>
    <script src="js/inspinia.js"></script>
    <script src="js/plugins/pace/pace.min.js"></script>
    <script src="js/plugins/iCheck/icheck.min.js"></script>
        <script>
            $(document).ready(function () {
                $('.i-checks').iCheck({
                    checkboxClass: 'icheckbox_square-green',
                    radioClass: 'iradio_square-green',
                });
            });
        </script>
</body>
</html>
