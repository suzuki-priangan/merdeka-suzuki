<?php include "head.php" ?>
<body>
    <div id="wrapper">
    <?php include "nav.php" ?>
        <div id="page-wrapper" class="gray-bg">
        <div class="row border-bottom">
        <nav class="navbar navbar-static-top" role="navigation" style="margin-bottom: 0">
        <div class="navbar-header">
            <a class="navbar-minimalize minimalize-styl-2 btn btn-primary " href="#"><i class="fa fa-bars"></i> </a>
            <form role="search" class="navbar-form-custom" action="">
            </form>
        </div>
            <ul class="nav navbar-top-links navbar-right">
                <li>
                    <a href="../keluar.php">
                        <i class="fa fa-sign-out"></i> Log out
                    </a>
                </li>
            </ul>

        </nav>
        </div>
            <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-lg-10">
                    <h2>Detail Tradein Perusahaan</h2>
                    <ol class="breadcrumb">
                        <li>
                            <a href="../index.php">Home</a>
                        </li>
                        <li>
                            <a>Tradein Perusahaan</a>
                        </li>
                        <li class="active">
                            <strong>Detail Tradein Perusahaan</strong>
                        </li>
                    </ol>
                </div>
                <div class="col-lg-2">

                </div>
            </div>
        <div class="wrapper wrapper-content animated fadeInRight">
            <div class="row">

                <div class="col-lg-5">

                </div>
            </div>

            <div class="row">
                <div class="col-lg-4">

                </div>
        </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="ibox float-e-margins">
                        <div class="ibox-title">
                            <h5>Detail Tradein Perusahaan </h5>
                        </div>
                        <div class="ibox-content">
                        	
                        	<?php
                                include '../koneksi.php';
                                
                                $id_tradein = $_GET['id_tradein'];
                                $data = mysqli_query($con, "select * from tradein_perusahaan where id_tradein='$id_tradein'");
                                while($d = mysqli_fetch_array($data)){
                            ?>

                            <form action="" method="post" class="form-horizontal">
                                <input type="hidden" name="id_tradein" value="<?php echo $id_tradein; ?>">

                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Nama Mobil</label>
                                        <div class="col-sm-10">
                                            <?php echo $d['nama_mobil']; ?>
                                        </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Tipe Mobil</label>
                                        <div class="col-sm-10">
                                            <?php echo $d['tipe_mobil']; ?>
                                        </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Tahun</label>
                                        <div class="col-sm-10">
                                            <?php echo $d['tahun']; ?>
                                        </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Transmisi</label>
                                        <div class="col-sm-10">
                                            <?php echo $d['transmisi']; ?>
                                        </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Eksterior Depan</label>
                                        <div class="col-sm-10">
                                            <img src="../../img/tradein_perusahaan/<?php echo $d['eksterior_depan'];?>" width="100px" height="80px">
                                        </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Eksterior Belakang</label>
                                        <div class="col-sm-10">
                                            <img src="../../img/tradein_perusahaan/<?php echo $d['eksterior_belakang'];?>" width="100px" height="80px">
                                        </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Interior Depan</label>
                                        <div class="col-sm-10">
                                            <img src="../../img/tradein_perusahaan/<?php echo $d['interior_depan'];?>" width="100px" height="80px">
                                        </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Interior Belakang</label>
                                        <div class="col-sm-10">
                                            <img src="../../img/tradein_perusahaan/<?php echo $d['interior_belakang'];?>" width="100px" height="80px">
                                        </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Kilometer</label>
                                        <div class="col-sm-10">
                                            <img src="../../img/tradein_perusahaan/<?php echo $d['kilometer'];?>" width="100px" height="80px">
                                        </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">STNK</label>
                                        <div class="col-sm-10">
                                            <img src="../../img/tradein_perusahaan/<?php echo $d['stnk'];?>" width="100px" height="80px">
                                        </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Jenis Mobil</label>
                                        <div class="col-sm-10">
                                            <?php echo $d['jenis_mobil']; ?>
                                        </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Tipe</label>
                                        <div class="col-sm-10">
                                            <?php echo $d['tipe']; ?>
                                        </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Warna</label>
                                        <div class="col-sm-10">
                                            <?php echo $d['warna']; ?>
                                        </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Harga</label>
                                        <div class="col-sm-10">
                                            <?php echo $d['harga']; ?>
                                        </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Kabupaten</label>
                                        <div class="col-sm-10">
                                            <?php echo $d['kabupaten']; ?>
                                        </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Nama Perusahaan</label>
                                        <div class="col-sm-10">
                                            <?php echo $d['nama_perusahaan']; ?>
                                        </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Bidang Usaha</label>
                                        <div class="col-sm-10">
                                            <?php echo $d['bidang_usaha']; ?>
                                        </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Nomer Akta</label>
                                        <div class="col-sm-10">
                                            <?php echo $d['no_akta']; ?>
                                        </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Alamat</label>
                                        <div class="col-sm-10">
                                            <?php echo $d['alamat']; ?>
                                        </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Nomer Telpon</label>
                                        <div class="col-sm-10">
                                            <?php echo $d['no_tlpn']; ?>
                                        </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Bentuk Usaha</label>
                                        <div class="col-sm-10">
                                            <?php echo $d['bentuk_usaha']; ?>
                                        </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Tipe Transaksi</label>
                                        <div class="col-sm-10">
                                            <?php echo $d['tipe_transaksi']; ?>
                                        </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Setuju</label>
                                        <div class="col-sm-10">
                                            <?php echo $d['setuju']; ?>
                                        </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-4 col-sm-offset-2">
                                        <a href="index.php" class="btn btn-info"><i class="glyphicon glyphicon-chevron-left"></i> Back</a>
                                    </div>
                                </div>
                            </form>
                            <?php 
                            }
                            ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
            
            <?php include "footer.php" ?>

        </div>
        </div>

    <script src="js/jquery-3.1.1.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/plugins/metisMenu/jquery.metisMenu.js"></script>
    <script src="js/plugins/slimscroll/jquery.slimscroll.min.js"></script>
    <script src="js/inspinia.js"></script>
    <script src="js/plugins/pace/pace.min.js"></script>
    <script src="js/plugins/iCheck/icheck.min.js"></script>
        <script>
            $(document).ready(function () {
                $('.i-checks').iCheck({
                    checkboxClass: 'icheckbox_square-green',
                    radioClass: 'iradio_square-green',
                });
            });
        </script>
</body>
</html>
