<?php
 if(!isset($_SESSION)) 
    { 
        session_start(); 
    } 
require_once ("koneksi.php");
if (isset($_SESSION['adminsuzuki'])) {
session_start();
  header('Location: index.php');
  exit();
} else
if (isset($_POST['masuk']) == 'on') {
	// Memfilter Inputan
	if (!ctype_alnum($_POST['username']) OR !ctype_alnum($_POST['password'])){
		echo "<script>
                    alert('SQL Injection Detected');
                    window.location='login.php';
             </script>";
		} else {
			// Query SQL Untuk Mengambbil Data dari tabel admin
			$query = "SELECT * FROM user WHERE username='" .$_POST['username']. "' AND password='" .($_POST['password']). "'";
			if ($query = $con->query($query)) {
				if ($query->num_rows) {
					session_start();
            while ($data = $query->fetch_array()) {
                $_SESSION['adminsuzuki'] = true;
                $_SESSION['username'] = $data['username'];
                $_SESSION['nama'] = $data['nama'];
            }
            header('Location: index.php');
        } else {
            echo "
                <script>
                    alert('Username atau Password Salah!');
                    window.location='login.php';
                </script>
            ";
        }
    } else {
        echo "<script>alert('Gagal');</script>";
    }
}
}
?>
<html>
 
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>SUZUKI Priangan | Login Site</title>
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="font-awesome/css/font-awesome.css" rel="stylesheet">
    <link href="css/animate.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">
    <link rel="icon" href="../../img/logo2.png">
    <script>
    $(document).ready(function(){
        $('[data-toggle="tooltip"]').tooltip();
    });
    </script>
    <script type="text/javascript">
    function validate()
    {
    var error="";
    var name = document.getElementById( "nama" );
    if( name.value == "" )
    {
    error = " Username Harus Diisi ";
    document.getElementById( "error_para" ).innerHTML = error;
    return false;
    }
    var error="";
    var name = document.getElementById( "pass" );
    if( name.value == "" )
    {
    error = " Password Harus Diisi ";
    document.getElementById( "error_para" ).innerHTML = error;
    return false;
    }
    else
    {
    return true;
    }
    }
</script>
</head>

<body class="gray-bg">
    <div class="middle-box text-center loginscreen animated fadeInDown">
        <div>
            <br><br>
            <h3>SUZUKI PRIANGAN</h3>
            <p>
                Selamat Datang Di Halaman Login Sistem Suzuki
            </p>
            
            <p>Silahkan Masukan Username & Password</p>
            <hr>
            <form class="m-t" role="form" action="" method="post" id="login-form" class="login100-form validate-form" onsubmit="return validate(); ">
                <div class="form-group">
                    <input type="text" name="username" id="username" class="form-control" placeholder="Username" required="">
                </div>
                <div class="form-group">
                    <input type="password" name="password" id="password" class="form-control" placeholder="Password" required="">
                </div>
                <button type="submit" class="btn btn-primary block full-width m-b" name="masuk">Login</button>

                <a href="#"><small>Doa Orang Tua</small></a>
                <p class="text-muted text-center"><small>Adalah Sumber Kesuksesan</small></p>
            </form>
            <p class="m-t"> <small>Suzuki Priangan &copy; 2019</small> </p>
        </div>
    </div>
    <script src="js/jquery-3.1.1.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
</body>
</html>
