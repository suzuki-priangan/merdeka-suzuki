<?php
include('../koneksi.php');
// ambil data file
$filename 			= $_FILES["gambar"]["name"];
$file_basename 		= substr($filename, 0, strripos($filename, '.'));
$file_ext 			= substr($filename, strripos($filename, '.'));
$filesize 			= $_FILES["gambar"]["size"];
$allowed_file_types = array('.jpg','.png','.jpeg','.gif','.JPG','.JPEG','.PNG');

if (in_array($file_ext,$allowed_file_types) && ($filesize < 200000000))
{
	// Ubah nama file
	$newfilename = ($file_basename) . $file_ext;
	if (file_exists("../../img/stok/" . $newfilename))
	{
		// Jika file sudah ada
		echo "<script>window.alert('File sudah ada!'); window.location.href='tambah.php'</script>";
	}
	else
	{
		move_uploaded_file($_FILES["gambar"]["tmp_name"], "../../img/stok/" . $newfilename);

    // ambil data dari form
    $gambar 		= $newfilename;
    $jenis_mobil 	= $_POST['jenis_mobil'];
    $warna			= $_POST['warna'];
    $stok 			= $_POST['stok'];
    $tahun 			= $_POST['tahun'];

    // query
    $query = "INSERT INTO `stok` (`id_stok`, `jenis_mobil`, `gambar`, `warna`, `stok`, `tahun`) VALUES (NULL, '$jenis_mobil', '$gambar', '$warna', '$stok', '$tahun');";

    $hasil = mysqli_query($con, $query);

    // cek keberhasilan pendambahan data
    if ($hasil == true) {
      echo "<script>window.alert('Tambah Data Berhasil'); window.location.href='index.php'</script>";
    } else {
      echo "<script>window.alert('Tambah Data Gagal!'); window.location.href='tambah.php'</script>";
    }

	}
}
elseif (empty($file_basename))
{
	// file belum dipilih
	echo "<script>window.alert('Pilih File Untuk Diunggah'); window.location.href='tambah.php'</script>";
}
elseif ($filesize > 2000000000)
{
	// ukuran file terlalu besar
	echo "<script>window.alert('File Terlalu Besar'); window.location.href='tambah.php'</script>";
}
else
{
	// format file bukan gambar
	echo "<script>window.alert('Format File Salah'); window.location.href='tambah.php'</script>";
	unlink($_FILES["gambar"]["tmp_name"]);
}
?>