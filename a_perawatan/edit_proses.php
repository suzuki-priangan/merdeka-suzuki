<?php
include('../koneksi.php');
$id_perawatan = $_POST['id_perawatan'];

$filename           = $_FILES['img']["name"];
$file_basename      = substr($filename, 0, strripos($filename, '.'));
$file_ext           = substr($filename, strripos($filename, '.'));
$filesize           = $_FILES['img']["size"];
$allowed_file_types = array('.jpg','.png','.jpeg','.gif','.JPG','.JPEG');

if (in_array($file_ext,$allowed_file_types) && ($filesize < 200000000))
{
    // Ubah nama file
    $newfilename = ($file_basename) . $file_ext;
    if (file_exists("../../img/perawatan/" . $newfilename))
    {
        // Jika file sudah ada
        echo "<script>window.alert('File Sudah Ada'); window.location.href='edit.php'</script>";
    }
    else
    {
        move_uploaded_file($_FILES["img"]["tmp_name"], "../../img/perawatan/" . $newfilename);

    // ambil data dari form
    $img            = $newfilename;
    $merk 			= $_POST['merk'];
    $kegunaan		= $_POST['kegunaan'];
    $jumlah 		= $_POST['jumlah'];
    $harga 			= $_POST['harga'];

    // query
    $query = "UPDATE perawatan SET merk='$merk', kegunaan='$kegunaan', img='$img', jumlah='$jumlah', harga='$harga' WHERE id_perawatan='$id_perawatan'";

    $hasil = mysqli_query($con, $query);

    // cek keberhasilan pendambahan data
    if ($hasil == true) {
      echo "<script>window.alert('Data Berhasil Di Update'); window.location.href='index.php'</script>";
    } else {
      echo "<script>window.alert('Data Gagal Di Update'); window.location.href='edit.php'</script>";
    }

    }
}
elseif (empty($filename))
{
    $merk 			= $_POST['merk'];
    $kegunaan		= $_POST['kegunaan'];
    $jumlah 		= $_POST['jumlah'];
    $harga 			= $_POST['harga'];
    $img            = $_POST['img'];
    
    $query  = "UPDATE perawatan SET merk='$merk', kegunaan='$kegunaan', jumlah='$jumlah', harga='$harga', img='$img' WHERE id_mobil='$id_mobil'";

$hasil = mysqli_query($con, $query);
    if ($hasil == true) {
      echo "<script>window.alert('Data Berhasil Di Update'); window.location.href='index.php'</script>";
    } else {
      echo "<script>window.alert('Data Gagal Di Update'); window.location.href='edit.php'</script>";
    }
    
}
elseif ($filesize > 2000000000)
{
    // ukuran file terlalu besar
    echo "<script>window.alert('File yang diunggah terlalu besar!'); window.location.href='edit.php?id_perawatan=$id_perawatan'</script>";
}
else
{
    // format file bukan gambar
    echo "<script>window.alert('Format file salah'); window.location.href='edit.php?id_perawatan=$id_perawatan'</script>";
    unlink($_FILES["img"]["tmp_name"]);
}