<?php
include('../koneksi.php');
// ambil data file
$filename 			= $_FILES["img"]["name"];
$file_basename 		= substr($filename, 0, strripos($filename, '.'));
$file_ext 			= substr($filename, strripos($filename, '.'));
$filesize 			= $_FILES["img"]["size"];
$allowed_file_types = array('.jpg','.png','.jpeg','.gif','.JPG','.JPEG  ');

if (in_array($file_ext,$allowed_file_types) && ($filesize < 200000000))
{
	// Ubah nama file
	$newfilename = md5($file_basename) . $file_ext;
	if (file_exists("../../img/perawatan/" . $newfilename))
	{
		// Jika file sudah ada
		echo "<script>window.alert('File sudah ada!'); window.location.href='tambah.php'</script>";
	}
	else
	{
		move_uploaded_file($_FILES["img"]["tmp_name"], "../../img/perawatan/" . $newfilename);

    // ambil data dari form
    $img 		= $newfilename;
    $merk 		= $_POST['merk'];
    $kegunaan	= $_POST['kegunaan'];
    $jumlah 	= $_POST['jumlah'];
    $harga 		= $_POST['harga'];

    // query
    $query = "INSERT INTO `perawatan` (`id_perawatan`, `merk`, `kegunaan`, `img`, `jumlah`, `harga`) VALUES (NULL, '$merk', '$kegunaan', '$img', '$jumlah', '$harga');";

    $hasil = mysqli_query($con, $query);

    // cek keberhasilan pendambahan data
    if ($hasil == true) {
      echo "<script>window.alert('Tambah Data Perawatan Berhasil'); window.location.href='index.php'</script>";
    } else {
      echo "<script>window.alert('Tambah Data Perawatan Gagal!'); window.location.href='tambah.php'</script>";
    }

	}
}
elseif (empty($file_basename))
{
	// file belum dipilih
	echo "<script>window.alert('Pilih file untuk diunggah'); window.location.href='tambah.php'</script>";
}
elseif ($filesize > 2000000000)
{
	// ukuran file terlalu besar
	echo "<script>window.alert('File Terlalu Besar!'); window.location.href='tambah.php'</script>";
}
else
{
	// format file bukan gambar
	echo "<script>window.alert('Format File Salah'); window.location.href='tambah.php'</script>";
	unlink($_FILES["img"]["tmp_name"]);
}
?>