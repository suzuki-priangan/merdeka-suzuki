<?php
include('../koneksi.php');
// ambil data file
$filename 			= $_FILES["img_artikel"]["name"];
$file_basename 		= substr($filename, 0, strripos($filename, '.'));
$file_ext 			= substr($filename, strripos($filename, '.'));
$filesize 			= $_FILES["img_artikel"]["size"];
$allowed_file_types = array('.jpg','.png','.jpeg','.gif','.JPG','.JPEG  ');

if (in_array($file_ext,$allowed_file_types) && ($filesize < 200000000))
{
	// Ubah nama file
	$newfilename = md5($file_basename) . $file_ext;
	if (file_exists("../../img/artikel/" . $newfilename))
	{
		// Jika file sudah ada
		echo "<script>window.alert('File sudah ada!'); window.location.href='tambah.php'</script>";
	}
	else
	{
		move_uploaded_file($_FILES["img_artikel"]["tmp_name"], "../../img/artikel/" . $newfilename);

    // ambil data dari form
    $img_artikel 	= $newfilename;
    $judul 			= $_POST['judul'];
    $isi 			= $_POST['isi'];
    $penulis		= $_POST['penulis'];
    $tgl_tulis 		= $_POST['tgl_tulis'];

    // query
    $query = "INSERT INTO `artikel` (`id_artikel`, `judul`, `isi`, `penulis`, `img_artikel`, `tgl_tulis`) VALUES (NULL, '$judul', '$isi', '$penulis', '$img_artikel', '$tgl_tulis');";

    $hasil = mysqli_query($con, $query);

    // cek keberhasilan pendambahan data
    if ($hasil == true) {
      echo "<script>window.alert('Tambah Artikel Berhasil'); window.location.href='index.php'</script>";
    } else {
      echo "<script>window.alert('Tambah Artikel Gagal!'); window.location.href='index.php'</script>";
    }

	}
}
elseif (empty($file_basename))
{
	// file belum dipilih
	echo "<script>window.alert('Pilih file untuk diunggah'); window.location.href='tambah.php'</script>";
}
elseif ($filesize > 2000000000)
{
	// ukuran file terlalu besar
	echo "<script>window.alert('File yang diunggah terlalu besar!'); window.location.href='tambah.php'</script>";
}
else
{
	// format file bukan gambar
	echo "<script>window.alert('Format file salah'); window.location.href='tambah.php'</script>";
	unlink($_FILES["img_artikel"]["tmp_name"]);
}
?>