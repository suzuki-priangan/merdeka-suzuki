<?php include "head.php" ?>
<body>
    <div id="wrapper">
    <?php include "nav.php" ?>
        <div id="page-wrapper" class="gray-bg">
        <div class="row border-bottom">
        <nav class="navbar navbar-static-top" role="navigation" style="margin-bottom: 0">
        <div class="navbar-header">
            <a class="navbar-minimalize minimalize-styl-2 btn btn-primary " href="#"><i class="fa fa-bars"></i> </a>
            <form role="search" class="navbar-form-custom" action="">
            </form>
        </div>
            <ul class="nav navbar-top-links navbar-right">
                <li>
                    <a href="../keluar.php">
                        <i class="fa fa-sign-out"></i> Log out
                    </a>
                </li>
            </ul>

        </nav>
        </div>
            <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-lg-10">
                    <h2>Edit Booking Service</h2>
                    <ol class="breadcrumb">
                        <li>
                            <a href="../index.php">Home</a>
                        </li>
                        <li>
                            <a>Booking Service</a>
                        </li>
                        <li class="active">
                            <strong>Edit Booking Service</strong>
                        </li>
                    </ol>
                </div>
                <div class="col-lg-2">

                </div>
            </div>
        <div class="wrapper wrapper-content animated fadeInRight">
            <div class="row">

                <div class="col-lg-5">

                </div>
            </div>

            <div class="row">
                <div class="col-lg-4">

                </div>
        </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="ibox float-e-margins">
                        <div class="ibox-title">
                            <h5>Edit Booking Service </h5>
                        </div>
                        <div class="ibox-content">
                        	
                        	<?php
                                include '../koneksi.php';
                                
                                $id_bs = $_GET['id_bs'];
                                $data = mysqli_query($con, "select * from booking_service where id_bs='$id_bs'");
                                while($d = mysqli_fetch_array($data)){
                            ?>

                            <form action="edit_proses.php" method="post" class="form-horizontal">
                            	<input type="hidden" name="id_bs" value="<?php echo $id_bs; ?>">

                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Nama</label>
                                        <div class="col-sm-10">
                                            <input type="text" name="nama" class="form-control" value="<?php echo $d['nama']; ?>">
                                        </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Email</label>
                                        <div class="col-sm-10">
                                            <input type="email" name="email" class="form-control" value="<?php echo $d['email']; ?>">
                                        </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Handphone</label>
                                        <div class="col-sm-10">
                                            <input type="text" name="hp" class="form-control" value="<?php echo $d['hp']; ?>">
                                        </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Whatsapp</label>
                                        <div class="col-sm-10">
                                            <input type="text" name="wa" class="form-control" value="<?php echo $d['wa']; ?>">
                                        </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Merk</label>
                                        <div class="col-sm-10">
                                            <input type="text" name="merk" class="form-control" value="<?php echo $d['merk']; ?>">
                                        </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Tipe</label>
                                        <div class="col-sm-10">
                                            <input type="text" name="tipe" class="form-control" value="<?php echo $d['tipe']; ?>">
                                        </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">No Vin</label>
                                        <div class="col-sm-10">
                                            <input type="text" name="no_vin" class="form-control" value="<?php echo $d['no_vin']; ?>">
                                        </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Nomor Polisi</label>
                                        <div class="col-sm-10">
                                            <input type="text" name="no_pol" class="form-control" value="<?php echo $d['no_pol']; ?>">
                                        </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Nama STNK</label>
                                        <div class="col-sm-10">
                                            <input type="text" name="nama_stnk" class="form-control" value="<?php echo $d['nama_stnk']; ?>">
                                        </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Layanan</label>
                                        <div class="col-sm-10">
                                            <input type="text" name="layanan" class="form-control"value="<?php echo $d['layanan']; ?>">
                                        </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Provinsi</label>
                                        <div class="col-sm-10">
                                            <input type="text" name="provinsi" class="form-control"value="<?php echo $d['provinsi']; ?>">
                                        </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Kabupaten</label>
                                        <div class="col-sm-10">
                                            <input type="text" name="kabupaten" class="form-control"value="<?php echo $d['kabupaten']; ?>">
                                        </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Kecamatan</label>
                                        <div class="col-sm-10">
                                            <input type="text" name="kecamatan" class="form-control"value="<?php echo $d['kecamatan']; ?>">
                                        </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Tanggal</label>
                                        <div class="col-sm-10">
                                            <input type="date" name="tgl" class="form-control" value="<?php echo $d['tgl']; ?>">
                                        </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Jam Service</label>
                                        <div class="col-sm-10">
                                            <input type="time" name="jam_service" class="form-control" value="<?php echo $d['jam_service']; ?>">
                                        </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-4 col-sm-offset-2">
                                        <button class="btn btn-primary" name="save" type="submit"><i class="glyphicon glyphicon-saved"></i> Update</button>
                                        <a href="index.php" type="submit" class="btn btn-warning"><i class="glyphicon glyphicon-remove"></i> Cencel</a>
                                    </div>
                                </div>
                            </form>
                            <?php 
                            }
                            ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
            
            <?php include "footer.php" ?>

        </div>
        </div>

    <script src="js/jquery-3.1.1.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/plugins/metisMenu/jquery.metisMenu.js"></script>
    <script src="js/plugins/slimscroll/jquery.slimscroll.min.js"></script>
    <script src="js/inspinia.js"></script>
    <script src="js/plugins/pace/pace.min.js"></script>
    <script src="js/plugins/iCheck/icheck.min.js"></script>
        <script>
            $(document).ready(function () {
                $('.i-checks').iCheck({
                    checkboxClass: 'icheckbox_square-green',
                    radioClass: 'iradio_square-green',
                });
            });
        </script>
</body>
</html>
