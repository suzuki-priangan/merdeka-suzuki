<?php 
// menghubungkan dengan koneksi
include '../koneksi.php';
// menghubungkan dengan library excel reader
include "excel_reader.php";
?>

<?php
// upload file xls
$target = basename($_FILES['file']['name']) ;
move_uploaded_file($_FILES['file']['tmp_name'], $target);

// beri permisi agar file xls dapat di baca
chmod($_FILES['file']['name'],0777);

// mengambil isi file xls
$data = new Spreadsheet_Excel_Reader($_FILES['file']['name'],false);
// menghitung jumlah baris data yang ada
$jumlah_baris = $data->rowcount($sheet_index=0);

// jumlah default data yang berhasil di import
$berhasil = 0;
for ($i=2; $i<=$jumlah_baris; $i++){

	// menangkap data dan memasukkan ke variabel sesuai dengan kolumnya masing-masing
	$id_mobil    	= $data->val($i, 1);
	$jenis_mobil    = $data->val($i, 2);
	$tipe   		= $data->val($i, 3);
	$tenor  		= $data->val($i, 4);
	$harga  		= $data->val($i, 5);
	$total_dp  		= $data->val($i, 6);
	$angsuran  		= $data->val($i, 7);
	$harga_2  		= $data->val($i, 8);
	$total_dp_2  	= $data->val($i, 9);
	$angsuran_2  	= $data->val($i, 10);
	$harga_3  		= $data->val($i, 11);
	$total_dp_3  	= $data->val($i, 12);
	$angsuran_3  	= $data->val($i, 13);
	$harga_4  		= $data->val($i, 14);
	$total_dp_4  	= $data->val($i, 15);
	$angsuran_4  	= $data->val($i, 16);
	$harga_5  		= $data->val($i, 17);
	$total_dp_5  	= $data->val($i, 18);
	$angsuran_5  	= $data->val($i, 19);

	if($id_mobil != "" && $jenis_mobil != "" && $tipe != "" && $tenor != "" && $harga != "" && $total_dp != "" && $angsuran != "" && $harga_2 != "" && $total_dp_2 != "" && $angsuran_2 != "" && $harga_3 != "" && $total_dp_3 != "" && $angsuran_3 != "" && $harga_4 != "" && $total_dp_4 != "" && $angsuran_4 != ""&& $harga_5 != "" && $total_dp_5 != "" && $angsuran_5 != ""){
		// input data ke database (table data_pegawai)
		mysqli_query($con,"INSERT into dp values('','$id_mobil','$jenis_mobil','$tipe','$tenor','$harga','$total_dp','$angsuran','$harga_2','$total_dp_2','$angsuran_2','$harga_3','$total_dp_3','$angsuran_3','$harga_4','$total_dp_4','$angsuran_4','$harga_5','$total_dp_5','$angsuran_5')");
		$berhasil++;
	}
}

// hapus kembali file .xls yang di upload tadi
unlink($_FILES['file']['name']);

// alihkan halaman ke index.php
header("location:index.php?berhasil=$berhasil");
?>