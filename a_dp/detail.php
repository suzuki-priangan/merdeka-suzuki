<?php include "../koneksi.php" ?>
<?php include "head.php" ?>

<body>
    <div id="wrapper">
    <?php include "nav.php" ?>
        <div id="page-wrapper" class="gray-bg">
        <div class="row border-bottom">
        <nav class="navbar navbar-static-top" role="navigation" style="margin-bottom: 0">
        <div class="navbar-header">
            <a class="navbar-minimalize minimalize-styl-2 btn btn-primary " href="#"><i class="fa fa-bars"></i> </a>
            <form role="search" class="navbar-form-custom" action="">
            </form>
        </div>
            <ul class="nav navbar-top-links navbar-right">
                <li>
                    <a href="../keluar.php">
                        <i class="fa fa-sign-out"></i> Log out
                    </a>
                </li>
            </ul>

        </nav>
        </div>
            <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-lg-10">
                    <h2>Detail DP</h2>
                    <ol class="breadcrumb">
                        <li>
                            <a href="../index.php">Home</a>
                        </li>
                        <li>
                            <a>Detail</a>
                        </li>
                        <li class="active">
                            <strong>Detail DP</strong>
                        </li>
                    </ol>
                </div>
                <div class="col-lg-2">

                </div>
            </div>
        <div class="wrapper wrapper-content animated fadeInRight">
            <div class="row">

                <div class="col-lg-5">

                </div>
            </div>

            <div class="row">
                <div class="col-lg-4">

                </div>
        </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="ibox float-e-margins">
                        <div class="ibox-title">
                            <h5>Detail DP</h5>
                        </div>
                        <div class="ibox-content">
                        	
                        	<?php
                                include '../koneksi.php';
                                
                                $id_dp = $_GET['id_dp'];
                                $data = mysqli_query($con, "select * from dp where id_dp='$id_dp'");
                                while(@$d = mysqli_fetch_array($data)){
                            ?>

                            <form action="#" method="post" class="form-horizontal" enctype="multipart/form-data">
                            	<input type="hidden" name="id_dp" value="<?php echo $id_dp; ?>">

                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Jenis Mobil</label>
                                    <div class="col-sm-10">
                                        <input type="text" name="jenis_mobil" class="form-control" value="<?php echo $d['jenis_mobil']; ?>" readonly>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Tipe</label>
                                    <div class="col-sm-10">
                                        <input type="text" name="tipe" class="form-control" value="<?php echo $d['tipe']; ?>" readonly>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Tenor</label>
                                        <div class="col-sm-10">
                                        <input type="text" name="tenor" class="form-control" value="<?php echo $d['tenor']; ?>" readonly>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Harga</label>
                                    <div class="col-sm-10">
                                        <input type="text" name="harga" class="form-control" value="<?php echo $d['harga']; ?>" readonly>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Total DP</label>
                                    <div class="col-sm-10">
                                        <input type="text" name="total_dp" class="form-control" value="<?php echo $d['total_dp']; ?>" readonly>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Angsuran</label>
                                    <div class="col-sm-10">
                                        <input type="text" name="angsuran" class="form-control" value="<?php echo $d['angsuran']; ?>" readonly>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Harga 2</label>
                                    <div class="col-sm-10">
                                        <input type="text" name="harga_2" class="form-control" value="<?php echo $d['harga_2']; ?>" readonly>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Total DP 2</label>
                                    <div class="col-sm-10">
                                        <input type="text" name="total_dp_2" class="form-control" value="<?php echo $d['total_dp_2']; ?>" readonly>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Angsuran 2</label>
                                    <div class="col-sm-10">
                                        <input type="text" name="angsuran_2" class="form-control" value="<?php echo $d['angsuran_2']; ?>" readonly>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Harga 3</label>
                                    <div class="col-sm-10">
                                        <input type="text" name="harga_3" class="form-control" value="<?php echo $d['harga_3']; ?>" readonly>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Total DP 3</label>
                                    <div class="col-sm-10">
                                        <input type="text" name="total_dp_3" class="form-control" value="<?php echo $d['total_dp_3']; ?>" readonly>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Angsuran 3</label>
                                    <div class="col-sm-10">
                                        <input type="text" name="angsuran_3" class="form-control" value="<?php echo $d['angsuran_3']; ?>" readonly>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Harga 4</label>
                                    <div class="col-sm-10">
                                        <input type="text" name="harga_4" class="form-control" value="<?php echo $d['harga_4']; ?>" readonly>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Total DP 4</label>
                                    <div class="col-sm-10">
                                        <input type="text" name="total_dp_4" class="form-control" value="<?php echo $d['total_dp_4']; ?>" readonly>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Angsuran 4</label>
                                    <div class="col-sm-10">
                                        <input type="text" name="angsuran_4" class="form-control" value="<?php echo $d['angsuran_4']; ?>" readonly>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Harga 5</label>
                                    <div class="col-sm-10">
                                        <input type="text" name="harga_5" class="form-control" value="<?php echo $d['harga_5']; ?>" readonly>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Total DP 5</label>
                                    <div class="col-sm-10">
                                        <input type="text" name="total_dp_5" class="form-control" value="<?php echo $d['total_dp_5']; ?>" readonly>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Angsuran 5</label>
                                    <div class="col-sm-10">
                                        <input type="text" name="angsuran_5" class="form-control" value="<?php echo $d['angsuran_5']; ?>" readonly>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-4 col-sm-offset-2">
                                        <a href="index.php" type="submit" class="btn btn-primary"><i class="glyphicon glyphicon-chevron-left"></i> Kembali</a>
                                    </div>
                                </div>
                            </form>
                            <?php 
                            }
                            ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
            
            <?php include "footer.php" ?>

        </div>
        </div>

    <script src="js/jquery-3.1.1.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/plugins/metisMenu/jquery.metisMenu.js"></script>
    <script src="js/plugins/slimscroll/jquery.slimscroll.min.js"></script>
    <script src="js/inspinia.js"></script>
    <script src="js/plugins/pace/pace.min.js"></script>
    <script src="js/plugins/iCheck/icheck.min.js"></script>
        <script>
            $(document).ready(function () {
                $('.i-checks').iCheck({
                    checkboxClass: 'icheckbox_square-green',
                    radioClass: 'iradio_square-green',
                });
            });
        </script>
</body>
</html>
