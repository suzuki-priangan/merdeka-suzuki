<?php
include('../koneksi.php');
// ambil data file
$filename 			= $_FILES["gambar"]["name"];
$file_basename 		= substr($filename, 0, strripos($filename, '.'));
$file_ext 			= substr($filename, strripos($filename, '.'));
$filesize 			= $_FILES["gambar"]["size"];
$allowed_file_types = array('.jpg','.png','.PNG','.jpeg','.gif','.JPG','.JPEG  ');

if (in_array($file_ext,$allowed_file_types) && ($filesize < 200000000))
{
	// Ubah nama file
	$newfilename = md5($file_basename) . $file_ext;
	if (file_exists("../../img/testimoni/" . $newfilename))
	{
		// Jika file sudah ada
		echo "<script>window.alert('File sudah ada!'); window.location.href='tambah.php'</script>";
	}
	else
	{
		move_uploaded_file($_FILES["gambar"]["tmp_name"], "../../img/testimoni/" . $newfilename);

    // ambil data dari form
    $gambar 		= $newfilename;
    $nama_konsumen 	= $_POST['nama_konsumen'];
    $isi 			= $_POST['isi'];
    $alamat 		= $_POST['alamat'];

    // query
    $query = "INSERT INTO `testimoni` (`id_testimoni`, `nama_konsumen`, `isi`, `gambar`, `alamat`) VALUES (NULL, '$nama_konsumen', '$isi', '$gambar', '$alamat');";

    $hasil = mysqli_query($con, $query);

    // cek keberhasilan pendambahan data
    if ($hasil == true) {
      echo "<script>window.alert('Tambah Testimoni Berhasil'); window.location.href='index.php'</script>";
    } else {
      echo "<script>window.alert('Tambah Testimoni Gagal!'); window.location.href='tambah.php'</script>";
    }

	}
}
elseif (empty($file_basename))
{
	// file belum dipilih
	echo "<script>window.alert('Pilih file untuk diunggah'); window.location.href='tambah.php'</script>";
}
elseif ($filesize > 2000000000)
{
	// ukuran file terlalu besar
	echo "<script>window.alert('File Terlalu Besar!'); window.location.href='tambah.php'</script>";
}
else
{
	// format file bukan gambar
	echo "<script>window.alert('Format File Salah'); window.location.href='tambah.php'</script>";
	unlink($_FILES["gambar"]["tmp_name"]);
}
?>